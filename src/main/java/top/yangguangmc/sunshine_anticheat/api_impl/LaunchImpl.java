package top.yangguangmc.sunshine_anticheat.api_impl;

import java.util.logging.Logger;

public final class LaunchImpl {
    public static final String NAME = "SunshineAntiCheat";

    public static void main(String[] args) {
        Logger logger = Logger.getLogger(NAME);
        logger.severe("Wrong method of launching!");
        logger.severe("This is a bukkit plugin! You should put it into the \"plugins\" folder of " +
                "the server directory instead of launch it directly.");
        javax.swing.JOptionPane.showMessageDialog(null, "This is a bukkit plugin!\n" +
                "You should put it into the \"plugins\" folder of the server directory\n" +
                "instead of launch it directly.", NAME, javax.swing.JOptionPane.ERROR_MESSAGE);
        System.exit(1);
    }
}
