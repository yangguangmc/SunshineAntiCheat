package top.yangguangmc.sunshine_anticheat.api_impl;

import me.clip.placeholderapi.expansion.PlaceholderExpansion;
import org.bukkit.OfflinePlayer;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import top.yangguangmc.sunshine_anticheat.SunshineAntiCheat;

public final class PlaceholderExpansionImpl extends PlaceholderExpansion {

    @Override
    public @NotNull String getIdentifier() {
        return "sunshineac";
    }

    @Override
    public @NotNull String getAuthor() {
        return SunshineAntiCheat.AUTHOR;
    }

    @Override
    public @NotNull String getVersion() {
        return SunshineAntiCheat.VERSION;
    }

    @Override
    public boolean persist() {
        return true;
    }

    @Override
    public @Nullable String onRequest(OfflinePlayer player, @NotNull String params) {
        //TODO
        return null;
    }
}
