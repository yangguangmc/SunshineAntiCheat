package top.yangguangmc.sunshine_anticheat.events;

import org.bukkit.event.HandlerList;
import org.bukkit.event.server.ServerEvent;

/**
 * Calls on each tick of the server.
 * This event is synchronized (on the primary thread of the server).
 */
public class ServerTickEvent extends ServerEvent {
    public static volatile long tickId = 0;
    private static final HandlerList handlers = new HandlerList();
    private final long id;

    public ServerTickEvent() {
        this.id = tickId;
    }

    public long getId() {
        return id;
    }

    @Override
    public HandlerList getHandlers() {
        return handlers;
    }

    public static HandlerList getHandlerList() {
        return handlers;
    }
}
