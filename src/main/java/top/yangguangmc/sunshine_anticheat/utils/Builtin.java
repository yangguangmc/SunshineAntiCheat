package top.yangguangmc.sunshine_anticheat.utils;

import top.yangguangmc.sunshine_anticheat.SunshineAntiCheat;

import java.util.logging.Logger;

/**
 * A simple util class to simplify coding.
 * <b>Do not use this outside SunshineAntiCheat project!</b>
 */
public final class Builtin {
    public static SunshineAntiCheat ssac = SunshineAntiCheat.getInstance();
    public static Logger logger = ssac.getLogger();

    private Builtin() {
    }

    public static int sqr(int i) {
        return i * i;
    }

    public static long sqr(long l) {
        return l * l;
    }

    public static double sqr(double d) {
        return d * d;
    }

    public static double sqrt(double d) {
        if (d >= 0) {
            return Math.sqrt(d);
        } else throw new ArithmeticException("Sqrt by a negative num: " + d);
    }

    public static int round(double d) {
        if (d < Integer.MAX_VALUE && d > Integer.MIN_VALUE) {
            return (int) Math.round(d);
        } else throw new ArithmeticException("Integer overflow.");
    }

    public static int floor(double d) {
        if (d < Integer.MAX_VALUE && d > Integer.MIN_VALUE) {
            return (int) Math.floor(d);
        } else throw new ArithmeticException("Integer overflow.");
    }

    public static int ceil(double d) {
        if (d < Integer.MAX_VALUE && d > Integer.MIN_VALUE) {
            return (int) Math.ceil(d);
        } else throw new ArithmeticException("Integer overflow.");
    }

    public static int abs(int i) {
        return Math.abs(i);
    }

    public static double abs(double d) {
        return Math.abs(d);
    }
}
