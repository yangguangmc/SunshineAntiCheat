package top.yangguangmc.sunshine_anticheat.check.action;

public enum ActionType {
    CMD(3),
    PLAYER_CMD(4),
    MSG(1),
    ALERT(0),
    BROADCAST(2);
    private final int performPriority;

    ActionType(int performPriority) {
        this.performPriority = performPriority;
    }

    public int getPerformPriority() {
        return performPriority;
    }
}
