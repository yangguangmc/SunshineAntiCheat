package top.yangguangmc.sunshine_anticheat.check.checks.world;

import com.github.retrooper.packetevents.event.PacketReceiveEvent;
import com.github.retrooper.packetevents.protocol.packettype.PacketType;
import com.github.retrooper.packetevents.protocol.world.BlockFace;
import com.github.retrooper.packetevents.wrapper.play.client.WrapperPlayClientPlayerBlockPlacement;
import org.bukkit.Location;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;
import top.yangguangmc.sunshine_anticheat.check.Check;
import top.yangguangmc.sunshine_anticheat.check.CheckCategory;

public class AirPlaceCheck extends Check {

    public AirPlaceCheck() {
        super("AirPlace", "air-place", CheckCategory.WORLD);
    }

    @Override
    public void onPacketReceive(PacketReceiveEvent event) {
        Player player = (Player) event.getPlayer();
        if (!validateCheckable(player)) return;
        if (event.getPacketType() == PacketType.Play.Client.PLAYER_BLOCK_PLACEMENT) {
            WrapperPlayClientPlayerBlockPlacement packet = new WrapperPlayClientPlayerBlockPlacement(event);
            Location loc1 = new Location(player.getWorld(), packet.getBlockPosition().getX(), packet.getBlockPosition().getY(), packet.getBlockPosition().getZ());
            Block towards = loc1.getBlock();
            if (!towards.isEmpty() && !towards.isLiquid()) return;
            BlockFace face = packet.getFace();
            if (face == BlockFace.OTHER) return;
            Location loc2 = loc1.clone().add(face.getModX(), face.getModY(), face.getModZ());
            if (!loc2.getBlock().isEmpty()) return;
            failCheck(player, 1, "Tried to place a block towards a block that doesn't have a collision box. Towards: {}[{}, {}, {}], face: {}, block: [{}, {}, {}]",
                    towards.getType(), loc1.getBlockX(), loc1.getBlockY(), loc1.getBlockZ(), face, loc2.getBlockX(), loc2.getBlockY(), loc2.getBlockZ());
        }
    }
}
